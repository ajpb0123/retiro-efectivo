// Amplio
$( function() {
    var percentage = 100;
    $(".progressbar-barAmplio").progressbar({ value: percentage });
    $(".valorGraficaAmplio").text(percentage + "%");
    
    $('input#checkboxAmplio1[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barAmplio").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barAmplio").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
    });

    $('input#checkboxAmplio2[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barAmplio").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barAmplio").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
    });
    $('input#checkboxAmplio3[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barAmplio").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barAmplio").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
    });
    $('input#checkboxAmplio4[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-bar").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-bar").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaAmplio").text(percentage + "%");
        }
    });
  });

  // Limitado
$( function() {
    var percentage = 100;
    $(".progressbar-barLimitado").progressbar({ value: percentage });
    $(".valorGraficaLimitado").text(percentage + "%");
    
    $('input#checkboxLimitado1[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });

    $('input#checkboxLimitado2[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });
    $('input#checkboxLimitado3[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });
    $('input#checkboxLimitado4[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });
    $('input#checkboxLimitado5[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });
    $('input#checkboxLimitado6[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barLimitado").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barLimitado").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaLimitado").text(percentage + "%");
        }
    });
  });

   // Limitado
$( function() {
    var percentage = 100;
    $(".progressbar-barBasico").progressbar({ value: percentage });
    $(".valorGraficaBasico").text(percentage + "%");
    
    $('input#checkboxBasico1[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });

    $('input#checkboxBasico2[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
    $('input#checkboxBasico3[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
    $('input#checkboxBasico4[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
    $('input#checkboxBasico5[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
    $('input#checkboxBasico6[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
    $('input#checkboxBasico7[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
          var verdadero = percentage + 10;
          $(".progressbar-barBasico").progressbar({ value: verdadero });
          percentage = verdadero;
          $(".valorGraficaBasico").text(percentage + "%");
        }
        else if($(this).is(":not(:checked)")){
          var falso = percentage - 10;
          $(".progressbar-barBasico").progressbar({ value: falso });
          percentage = falso;
          $(".valorGraficaBasico").text(percentage + "%");
        }
    });
  });